class MyComponent extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: 'open'});
        this.template = document.querySelector('#myTemplate');
        this.myTemplate = document.importNode(this.template.content, true);
    }

    connectedCallback() {
        this.shadowRoot.appendChild(this.myTemplate);
    }

    attributeChangedCallback(attr, oldVal, newVal) {

    }

    static get observedAttributes() {
        return []
    }
}

window.customElements.define('my-component', MyComponent);