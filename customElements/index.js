class MyComponent extends HTMLElement {

    connectedCallback() {
        console.log('desde conected');
        const label = document.createElement('p');
        label.innerHTML = this.name;
        this.appendChild(label);
    }



    constructor() {
        super();
        this.name = '';
    }

    attributeChangedCallback(name, oldValue, newValue) {
        //console.log(`nombre ${name} y oldValue ${oldValue} y nuevoValor ${newValue}`);
        if(name === 'name') {
            this.name = `Hola ${newValue}`;
        }
    }

    static get observedAttributes() {
        return [ 'name' ];
    }
}

window.customElements.define('my-component', MyComponent)