const button = document.querySelector('button');
button.addEventListener('click', () => {
    const template = document.querySelector('#myTemplate');
    const container = document.querySelector('#notification');

    const cloneTemplate = document.importNode(template.content, true);
    container.appendChild(cloneTemplate);
});